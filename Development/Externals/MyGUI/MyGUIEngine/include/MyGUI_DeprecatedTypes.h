/*
 * This source file is part of MyGUI. For the latest info, see http://mygui.info/
 * Distributed under the MIT License
 * (See accompanying file COPYING.MIT or copy at http://opensource.org/licenses/MIT)
 */

#ifndef __MYGUI_DEPRECATED_TYPES_H__
#define __MYGUI_DEPRECATED_TYPES_H__

namespace MyGUI
{
	template <typename Type>
	class MemberObsolete
	{
	public:
	};

} // namespace MyGUI

#endif // __MYGUI_DEPRECATED_TYPES_H__
