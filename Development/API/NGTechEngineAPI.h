#pragma once 

//***************************************************
#include "../Src/Common/CommonPrivate.h"
#include "../Src/Common/IWindow.h"
#include "../Src/Common/IRender.h"
#include "../Src/Common/IGame.h"
#include "../Src/Common/IScripting.h"

#include "../Src/Core/inc/coredll.h"
#include "../Src/Core/inc/SteamWorks_API.h"
#include "../Src/Core/inc/Log.h"
#include "../Src/Core/inc/ASYNCFile.h"
#include "../Src/Core/inc/Config.h"


#include "../Src/Engine/inc/Engine.h"
#include "../Src/Engine/inc/ALSystem.h"
#include "../Src/Engine/inc/PhysSystem.h"
#include "../Src/Engine/inc/LoadingScreen.h"

#include "../Src/Engine/inc/Scene.h"
#include "../Src/Engine/inc/Terrain.h"
#include "../Src/Engine/inc/Light.h"
#include "../Src/Engine/inc/Effect.h"
#include "../Src/Engine/inc/Cache.h"

#include "../Src/Engine/inc/Font.h"
#include "../Src/Engine/inc/GUI.h"
//***************************************************

//***************************************************
//Scripting
#include "../Engine/inc/WrappedScriptFunctions.h"

//***************************************************

//End user API
#include "../Engine/API/RenderAPI.h"